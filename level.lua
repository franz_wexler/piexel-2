local level = {}

  level.Level = {}; level.Level.__index = level.Level
  
  function level.newLevel(filepath)
    local self = {}; setmetatable(self, level.Level)
    
    self.theme = {baseColor = {130, 130, 135}, bgColor = {235, 235, 240}, playerColor = {62, 82, 245},
      brightColor = {223, 218, 224}, mediumColor = {190, 190, 190}, darkColora = {165, 165, 165},
      shadowColor = {225, 225, 230}}
    
    print("Loading level from "..filepath)
    
    filepath = filepath:gsub('%.lua$', '') -- remove .lua extension
    
    self.levelData = require(filepath)
    local mapstr
    if self.levelData.mapString ~= nil then
      mapstr = self.levelData.mapString
    elseif self.levelData[1] ~= nil then
      mapstr = self.levelData[1]
    end
    
    local grid = {} -- level stored as multidimensional array of chars ([y][x])
    for line in mapstr:gmatch('[^\n]+') do
      table.insert(grid, {})
      for char in string.iterchars(line) do
        table.insert(grid[#grid], char)
      end
    end
    
    self.blocks, self.platforms, self.spikes = levelloader.getStaticGeometry(grid)
    
    self.entrance, self.exit = levelloader.getEntranceAndExit(self, grid)
    self.player = nil
    self.entrance:handlePlayerSpawn(self)
    
    self.staticSegments, self.lightPosition = levelloader.getLightingData(self, grid)
    self.shadowSystem = shadows.newSystem(self.lightPosition, self.theme.shadowColor)
    
    self.baseMesh = levelloader.getMeshes(self, grid)
    
    self.playerDeathParticles = particles.newSystem({12, 12}, self.theme.playerColor)
    
    self.completed = false
    
    return self
  end
  
  function level.Level:update()
    self.player:update()
    self.entrance:update()
    self.exit:update()
    self.playerDeathParticles:update()
    
    self.shadowSystem:setSegments({self.staticSegments, self.player:getSegments(),
        self.playerDeathParticles:getSegments()})
    self.shadowSystem:update()
  end
  
  function level.Level:draw()
    love.graphics.setColor(self.theme.bgColor)
    love.graphics.rectangle('fill', 0, 0, 1024, 576)
    
    
    self.shadowSystem:draw()
  
    love.graphics.setColor(self.theme.baseColor)
    love.graphics.draw(self.baseMesh)
    
    self.entrance:draw()
    self.exit:draw()
    
    self.player:draw()
    
    self.playerDeathParticles:draw()
    
    self.shadowSystem:drawGradient()
  end
  
  function level.Level:complete()
    self.completed = true
    levelmanager.nextLevel()
  end

return level
