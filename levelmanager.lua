local levelmanager = {}
  
  local currentFolder, currentFile
  local currentLevel
  
  function levelmanager.loadFolder(path)
    currentFolder = path
    levelmanager.nextLevel()
  end
  
  function levelmanager.nextLevel()
    assert(currentFolder ~= nil)
    
    local lastFile = currentFile
    
    local filepaths = love.filesystem.getDirectoryItems(currentFolder)
    
    -- find next file in directory
    if lastFile == nil then
      currentFile = filepaths[1]
    else
      for i, filepath in ipairs(filepaths) do
        if filepath == lastFile then
          currentFile = filepaths[i + 1]
        end
      end
    end
    if currentFile == nil then
      print("All levels completed")
      currentFile = nil
      levelmanager.nextLevel()
      return
    end
    
    currentLevel = level.newLevel(currentFolder..currentFile)
    --transitionmanager.setState(currentLevel)
    game.level = currentLevel
    
    transitionmanager.setState(quotescreens.newQuoteScreen(currentLevel))
  end

return levelmanager
