local particles = {}

  particles.System = {}
  particles.System.__index = particles.System
  
  function particles.newSystem(size, color, gravity, friction, rotationSpeed, lifetime, saveSegments)
    -- some sensible defaults
    if size == nil then size = {16, 16} end
    if color == nil then color = {255, 0, 255} end
    if gravity == nil then gravity = 0.2 end
    if friction == nil then friction = 0.01 end
    if rotationSpeed == nil then rotationSpeed = 0.02 end
    if lifetime == nil then lifetime = 140 end
    if saveSegments == nil then saveSegments = true end
    
    local self = {}; setmetatable(self, particles.System)
    
    -- data common for all particles
    self.particleSize = size -- {width, height}
    self.particleColor = color -- {r, g, b}
    self.particleGravity = gravity -- in px/frame^2
    self.particleFriction = friction -- in px/frame^2
    self.particleRotationSpeed = rotationSpeed -- may be added or subtracted, see update()
    self.particleLifetime = lifetime -- in frames; life left in particles is represented by alpha
    self.saveSegments = saveSegments -- if true, system holds all segments for shadow calculations
    
    -- contains all particles data which is unique to that particle (velX, velY, x, y, angle, alpha)
    self.particles = {}
    
    -- particles.System uses Mesh for best performance
    local MESH_SIZE = 1024 * 6 -- needs 6 points per particle, 1024 seems like a reasonable limit
    self.mesh = love.graphics.newMesh(MESH_SIZE, 'triangles', 'stream')
    self.mesh:setDrawRange(1, 1) -- when system is initialized it is empty
    
    return self
  end
  
  function particles.System:update()
    -- local for easier access and better performance
    local ps = self.particles
    local pw, ph = unpack(self.particleSize)
    local frict = self.particleFriction
    local grav = self.particleGravity
    local rot = self.particleRotationSpeed
    local alphaDelta = 255 / self.particleLifetime
    local saveSeg = self.saveSegments
    
    
    -- update all particles
    for i, p in ipairs(ps) do
      -- acceleration
      p.velX = trunc(p.velX, frict)
      p.velY = p.velY + grav
      
      -- move particle
      p.x = p.x + p.velX
      p.y = p.y + p.velY
      
      -- rotate (to the right if flying to right else to the left)
      if p.velX > 0 then p.angle = p.angle + rot else
        p.angle = p.angle - rot end
      
      -- fade out
      p.alpha = p.alpha - alphaDelta
      
      -- delete if not on screen or not visible (alpha < 0)
      if p.x < -pw or p.x > 1024 + pw or
          p.y < -ph or p.y > 576 + ph or
          p.alpha < 0 then
        table.remove(ps, i)
      end
    end
    
    -- update mesh and shadow collision segments
    local v = {}
    local seg = {} -- unused if saveSeg is false
    for _, p in ipairs(ps) do
      -- get corners of particle
      local x1, y1, x2, y2, x3, y3, x4, y4 = particles.getRotatedSquarePoints(p.x, p.y, pw, ph, p.angle)
      
      -- every particle needs two triangles creating together square
      for _, c in ipairs({{x1, y1}, {x2, y2}, {x3, y3},
                          {x1, y1}, {x3, y3}, {x4, y4}}) do
        local x, y = unpack(c)
        v[#v + 1] = {x, y, nil, nil, nil, nil, nil, p.alpha}
      end
      
      if saveSeg then
        for _, s in ipairs({{x1, y1, x2, y2}, {x2, y2, x3, y3},
                            {x3, y3, x4, y4}, {x4, y4, x1, y1}}) do
          seg[#seg + 1] = s
        end
      end
    end
    self.mesh:setVertices(v)
    self.mesh:setDrawRange(1, math.max(1, #v))
    if saveSeg then
      self.segments = seg
    end
  end
  
  function particles.System:draw()
    -- thanks to complicated calculations in update(), this method just draws the mesh
    love.graphics.setColor(self.particleColor)
    love.graphics.draw(self.mesh)
  end
  
  function particles.System:getSegments()
    if not self.saveSegments then
      error("This particle system doesn't store segments", 2)
    end
    return self.segments
  end
  
  -- adds a single particle
  function particles.System:add(x, y, velX, velY, rotation, randomize)
    if velX == nil then velX = 0 end
    if velY == nil then velY = 0 end
    if rotation == nil then rotation = 0 end
    if randomize == nil then randomize = true end
    
    local p = {} -- new particle
    
    p.velX, p.velY = velX, velY
    p.x, p.y = x, y
    p.angle = 0
    p.alpha = 255
    if randomize then
      p.velX = p.velX + math.random() * math.randomdirection() * 0.3
      p.velY = p.velY + math.random() * math.randomdirection() * 0.3
      p.angle = math.random() * math.pi
    end
    
    -- welcome new friend, Mr. Particle
    table.insert(self.particles, p)
  end
  
  -- adds 9 particles aligned in 3x3 grid creating nice player death effect
  function particles.System:addPlayerDeathParticles(player)
    local px, py, pw, ph = player.x, player.y, player.w, player.h
    local rw, rh = unpack(self.particleSize)
    
    local lowx = px + rw / 2
    local midx = px + pw / 2
    local topx = px + pw - rw / 2
    local lowy = py + rh / 2
    local midy = py + ph / 2
    local topy = py + ph - rh / 2
    local lowv = -2
    local midv = 0
    local topv = 2
    
    -- the wall of code
    self:add(lowx, lowy, lowv, lowv)
    self:add(lowx, midy, lowv, midv)
    self:add(lowx, topy, lowv, topv)
    self:add(midx, lowy, midv, lowv)
    self:add(midx, midy, midv, midv)
    self:add(midx, topy, midv, topv)
    self:add(topx, lowy, topv, lowv)
    self:add(topx, midy, topv, midv)
    self:add(topx, topy, topv, topv)
  end
  
  -- helper functions for creating geometry
  local function rotatePoint(cx, cy, x, y, angle)
    local tx, ty = x - cx, y - cy
    return tx * math.cos(angle) - ty * math.sin(angle) + cx,
           tx * math.sin(angle) + ty * math.cos(angle) + cy
  end
  function particles.getRotatedSquarePoints(cx, cy, w, h, angle)
    local x1, y1 = rotatePoint(cx, cy, cx - w/2, cy - h/2, angle)
    local x2, y2 = rotatePoint(cx, cy, cx + w/2, cy - h/2, angle)
    local x3, y3 = rotatePoint(cx, cy, cx + w/2, cy + h/2, angle)
    local x4, y4 = rotatePoint(cx, cy, cx - w/2, cy + h/2, angle)
    return x1, y1, x2, y2, x3, y3, x4, y4
  end

return particles
