local levelloader = {}

  function levelloader.getStaticGeometry(grid)
    local blocks, platforms, spikes = {}, {}, {}
    
    local lastchar = nil
    for y, row in ipairs(grid) do
      for x, char in ipairs(row) do
        local px, py, pw, ph = (x - 1) * 16, (y - 1) * 16, 16, 16
        
        if table.contains({'#', '-', 'X'}, char) then
          local list
          if char == '#' then -- block
            list = blocks
          elseif char == '-' then -- platform
            list = platforms
            ph = 8
          elseif char == 'X' then -- spike
            list = spikes
          end
          
          if x > 1 and lastchar == char then
            -- if last character was e.g. '#', and current is '#' too, we can just add
            -- 16 to the width of last block
            local lasttile = table.last(list)
            lasttile[3] = lasttile[3] + pw
          else
            table.insert(list, {px, py, pw, ph})
          end
          
        elseif table.contains({'S', 'E'}, char) then
          px, py = px - 8, py - 48
          if char == 'S' then
            --entrance = doors.newEntrance(self, px, py)
          elseif char == 'E' then
            --exit = doors.newExit(self, px, py)
          end
        end
        
        lastchar = char
      end
    end
    
    rectangles.optimize(blocks)
    rectangles.optimize(spikes)
    
    return blocks, platforms, spikes
  end
  
  
  function levelloader.getEntranceAndExit(level, grid)
    local entrance, exit = nil, nil
    
    for y, row in ipairs(grid) do
      for x, char in ipairs(row) do
        local px, py = (x - 1) * 16 - 8, (y - 1) * 16 - 48
        if char == 'S' then
          entrance = doors.newEntrance(level, px, py)
        elseif char == 'E' then
          exit = doors.newExit(level, px, py)
        end
      end
    end
    
    return entrance, exit
  end
  
  
  function levelloader.getLightingData(level, grid)
    local staticSegments = {}
    local lightPosition = nil
    
    local function addSegment(x1, y1, x2, y2)
      table.insert(staticSegments, {x1, y1, x2, y2})
    end
    local function addRectangleSegments(x, y, w, h)
      for _, s in ipairs(shadows.getRectangleSegments(x, y, w, h)) do
        addSegment(unpack(s))
      end
    end
    local function addSpikeSegments(x1, y1, x2, y2, x3, y3)
      addSegment(x1, y1, x2, y2)
      addSegment(x2, y2, x3, y3)
    end
    
    for _, b in ipairs(level.blocks) do
      addRectangleSegments(unpack(b))
    end
    for _, p in ipairs(level.platforms) do
      addRectangleSegments(unpack(p))
    end
    
    for y, row in ipairs(grid) do
      for x, char in ipairs(row) do
        local sx, sy, sw, sh = (x - 1) * 16, (y - 1) * 16, 16, 16
        if char == 'L' then
          lightPosition = {sx + sw / 2, sy + sh / 2}
        elseif char == 'X' then
          if y == 1 or grid[y - 1][x] == '#' then
            addSpikeSegments(sx, sy, sx + sw / 2, sy + sh, sx + sw, sy)
          elseif y == #grid or grid[y + 1][x] == '#' then
            addSpikeSegments(sx, sy + sh, sx + sw / 2, sy, sx + sw, sy + sh)
          elseif x == 1 or grid[y][x - 1] == '#' then
            addSpikeSegments(sx, sy, sx + sw, sy + sh / 2, sx, sy + sh)
          else
            addSpikeSegments(sx + sw, sy, sx, sy + sh / 2, sx + sw, sy + sh)
          end
        end
      end
    end
    
    if lightPosition == nil then
      lightPosition = {512, 40} -- good default if no light position is set
    end
    
    return staticSegments, lightPosition
  end
  
  function levelloader.getMeshes(level, grid)
    local vertices = {} -- table with all points, every three create a triangle
    
    local function addRectangle(x, y, w, h)
      table.insert(vertices, {x,     y    }) -- 0, 0
      table.insert(vertices, {x + w, y    }) -- 1, 0
      table.insert(vertices, {x + w, y + h}) -- 1, 1
      
      table.insert(vertices, {x,     y    }) -- 0, 0
      table.insert(vertices, {x + w, y + h}) -- 1, 1
      table.insert(vertices, {x,     y + h}) -- 0, 1
    end
    local function addTriangle(x1, y1, x2, y2, x3, y3)
      table.insert(vertices, {x1, y1})
      table.insert(vertices, {x2, y2})
      table.insert(vertices, {x3, y3})
    end
    
    for _, b in ipairs(level.blocks) do
      addRectangle(unpack(b))
    end
    for _, p in ipairs(level.platforms) do
      addRectangle(unpack(p))
    end
    
    for y, row in ipairs(grid) do
      for x, char in ipairs(row) do
        if char == 'X' then
          local sx, sy, sw, sh = (x - 1) * 16, (y - 1) * 16, 16, 16
          if y == 1 or grid[y - 1][x] == '#' then
            -- roof spike
            addTriangle(sx, sy,
                        sx + sw, sy,
                        sx + sw / 2, sy + sh)
          elseif y == #grid or grid[y + 1][x] == '#' then
            -- ground spike
            addTriangle(sx, sy + sh,
                        sx + sw, sy + sh,
                        sx + sw / 2, sy)
          elseif x == 1 or grid[y][x - 1] == '#' then
            -- on wall to the left
            addTriangle(sx, sy,
                        sx + sw, sy + sh / 2,
                        sx, sy + sh)
          else
            -- on wall to the right
            addTriangle(sx + sw, sy,
                        sx, sy + sh / 2,
                        sx + sw, sy + sh)
          end
        end
      end
    end
    
    local baseMesh = love.graphics.newMesh(vertices, 'triangles', 'static')
    
    return baseMesh
  end
  
return levelloader

