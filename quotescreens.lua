local quotescreens = {}

  quotescreens.QuoteScreen = {}; quotescreens.QuoteScreen.__index = quotescreens.QuoteScreen
  
  function quotescreens.newQuoteScreen(level)
    local self = {}
    setmetatable(self, quotescreens.QuoteScreen)
    
    if level.levelData.quote ~= nil then
      self.message = translation.get(level.levelData.quote[translation.getLanguage()])
    else
      self.message = "nil: "..tostring(level.levelData)
    end
    self.level = level
    
    self.hasSetState = false
    
    self.textBlinkAnim = animations.newAnimation(255, 200, 300)
    self.hintTextBlinkAnim = animations.newAnimation(20, 100, 100)
    
    return self
  end
  
  function quotescreens.QuoteScreen:update()
    self.hintTextBlinkAnim:update()
    if self.hintTextBlinkAnim:hasEnded() then
      self.hintTextBlinkAnim:reverseDirection()
    end
    self.textBlinkAnim:update()
    if self.textBlinkAnim:hasEnded() then
      self.textBlinkAnim:reverseDirection()
    end
    
    if love.keyboard.isScancodeDown('return') and not self.hasSetState then
      transitionmanager.setState(self.level)
      self.hasSetState = true
    end
  end
  
  function quotescreens.QuoteScreen:draw()
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle('fill', 0, 0, 1024, 576)
    
    love.graphics.setColor(255, 255, 255, self.textBlinkAnim:getCurrentValue())
    letterbox.print(40, self.message)
    
    love.graphics.setColor(255, 255, 255, self.hintTextBlinkAnim:getCurrentValue())
    letterbox.print(14, translation.get("press Enter key"), nil, 500, nil, 76)
  end

return quotescreens
