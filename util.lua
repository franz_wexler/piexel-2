-- This file does its best to repair Lua
-- It is doomed to fail...

-- table.contains(self: table, value: any)
function table:contains(value)
  -- VERY SLOW
  for _, item in ipairs(self) do
    if item == value then
      return true
    end
  end
  return false
end


function string:iterchars()
  return self:gmatch(".")
end


function table:last()
  return self[#self]
end


function table:find(value)
  -- VERY SLOW
  for i, v in ipairs(self) do
    if v == value then
      return i
    end
  end
  return nil
end

function table:removeitem(value)
  -- VERY SLOW
  table.remove(self, table.find(self, value))
end


function table:qcopy()
  -- VERY SLOW
  return {unpack(self)}
end


-- trunc(original: number, step: number, target: number = 0)
function trunc(original, step, target)
  if target == nil then target = 0 end
  
  if original == target or
      (target - step < original and original < target + step) then
    return target
  elseif original < target then
    return original + step
  else
    return original - step
  end
end

function math.randomdirection()
  if math.random() < 0.5 then
    return -1
  else
    return 1
  end
end
