require 'util'

love = require 'love'

translation = require 'translation'

rectangles = require 'rectangles'
animations = require 'animations'
shadows = require 'shadows'

letterbox = require 'letterbox'
transitionmanager = require 'transitionmanager'
levelmanager = require 'levelmanager'
shadermanager = require 'shadermanager'
player = require 'player'
levelloader = require 'levelloader'
level = require 'level'
quotescreens = require 'quotescreens'
game = require 'game'
doors = require 'doors'

particles = require 'particles'


function love.load(arg)
  if arg ~= nil and table.contains(arg, '-debug') then
    --require('mobdebug').start()
  end
  game.load()
end

