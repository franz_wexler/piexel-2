local shadows = {}

  shadows.System = {}; shadows.System.__index = shadows.System
  function shadows.newSystem(lightPosition, color)
    local self = {}; setmetatable(self, shadows.System)
    
    self.lightPosition = lightPosition
    self.color = color
    
    self.mesh = love.graphics.newMesh(4096 * 6, 'triangles', 'stream')
    
    self.gradientMesh = shadows.getLightGradient(lightPosition)
    
    return self
  end
  
  function shadows.System:setSegments(segmentTables)
    self.segments = segmentTables
  end
  
  function shadows.System:update()
    local v = shadows.getShadowVertices(self.lightPosition, self.segments)
    self.mesh:setVertices(v)
    self.mesh:setDrawRange(1, math.max(1, #v))
  end
  
  function shadows.System:draw()
    love.graphics.setColor(self.color)
    love.graphics.draw(self.mesh)
  end
  
  function shadows.System:drawGradient()
    love.graphics.setColor(255, 255, 255, 100)
    love.graphics.draw(self.gradientMesh)
  end


  function shadows.getRectangleSegments(x, y, w, h)
    return {{x,     y,     x + w, y    },
            {x + w, y,     x + w, y + h},
            {x + w, y + h, x,     y + h},
            {x,     y + h, x,     y    }}
  end

  function shadows.getShadowVertices(lightPosition, segmentTables)
    -- segmentTables should have form of {{{beginX, beginY, endX, endY}, ...}, ...}
    
    local lx, ly = unpack(lightPosition)
    
    local v = {}
    for _, t in ipairs(segmentTables) do
      for _, s in ipairs(t) do
        local x1, y1, x2, y2 = unpack(s)
        local dx1, dy1 = x1 - lx, y1 - ly
        local rx1, ry1 = x1 + dx1 * 100, y1 + dy1 * 100
        local dx2, dy2 = x2 - lx, y2 - ly
        local rx2, ry2 = x2 + dx2 * 100, y1 + dy2 * 100
        
        local i = #v
        v[i + 1] = {x1, y1  }
        v[i + 2] = {x2, y2  }
        v[i + 3] = {rx2, ry2}
        
        v[i + 4] = {x1, y1  }
        v[i + 5] = {rx2, ry2}
        v[i + 6] = {rx1, ry1}
      end
    end
    
    return v
  end
  
  function shadows.getLightGradient(lightPosition, color, endColor, d)
    if color == nil then color = {0, 0, 0, 0} end
    if endColor == nil then endColor = {0, 0, 0, 255} end
    if d == nil then d = 1200 end
    
    local x, y = unpack(lightPosition)
    local r, g, b, a = unpack(color)
    local er, eg, eb, ea = unpack(endColor)
    
    local vc = 32 -- number of vertices
    
    local v = {{x, y, nil, nil, r, g, b, a}}
    for i=0, vc do
      local angle = (i / vc) * math.pi * 2
   
      local vx = math.cos(angle) * d
      local vy = math.sin(angle) * d
   
      table.insert(v, {x + vx, y + vy, nil, nil, er, eg, eb, ea})
    end
 
    return love.graphics.newMesh(v, 'fan')
  end

return shadows
