# Piexel 2

Designed as sequel to [Piexel](https://bitbucket.org/franz_wexler/piexel), Piexel 2 never has been finished.

It uses Lua with [LÖVE](https://bitbucket.org/rude/love).

Lua however made it very challenging to manage code. I abandon this project and most probably will never come to it again.