shadermanager = {}

  local shaders = {}
  
  function shadermanager.loadShaders()
    local files = love.filesystem.getDirectoryItems('shaders/')
    
    for _, file in ipairs(files) do
      shaders[file:match("[^.]+")] = love.graphics.newShader('shaders/'..file)
    end
  end
  
  function shadermanager.update()
    for _, shader in pairs(shaders) do
      pcall(function() shader:send('iGlobalTime', game.ticks / 60) end)
      pcall(function() shader:send('iRandom', math.random()) end)
    end
  end
  
  function shadermanager.setShader(shader)
    if shader ~= nil then
      shader = shaders[shader]
    end
    love.graphics.setShader(shader)
  end
  
return shadermanager