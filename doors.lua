local doors = {}

  doors.Door = {}; doors.Door.__index = doors.Door
  
  function doors.newDoor(level_, x, y)
    local self = {}; setmetatable(self, doors.Door)
    
    self.level = level_
    
    self.x, self.y, self.w, self.h = x, y, 32, 64
    self.wingW, self.wingH = 24, self.h
    
    self.open = false
    self.openingAnimation = animations.newAnimation(1, self.w / 2, 16)
    
    self.playerAbove = true
    
    return self
  end
  
  function doors.Door:update()
    self.openingAnimation:update()
  end
  
  function doors.Door:draw()
    love.graphics.setColor(self.level.theme.brightColor)
    love.graphics.rectangle('fill', self.x, self.y, self.w, self.h)
    
    love.graphics.setColor(self.level.theme.mediumColor)
    local v = self.openingAnimation:getCurrentValue()
    local dw, dh = self.wingW, self.wingH
    love.graphics.rectangle('fill', self.x + self.w / 2 - dw - v, self.y, dw, dh)
    love.graphics.rectangle('fill', self.x + self.w / 2 + v, self.y, dw, dh)
  end
  
  doors.Entrance = {}; doors.Entrance.__index = doors.Entrance
  setmetatable(doors.Entrance, doors.Door)
  
  function doors.newEntrance(level_, x, y)
    local self = doors.newDoor(level_, x, y); setmetatable(self, doors.Entrance)
    return self
  end
  
  function doors.Entrance:update()
    doors.Door.update(self)
    
    if self.openingAnimation:hasEnded() then
      self.playerAbove = true
    end
    if self.playerAbove then
      local p = self.level.player
      if not rectangles.collide(self.x, self.y, self.w, self.h, p.x, p.y, p.w, p.h) then
        self.openingAnimation:setDirection('backward')
      end
    end
  end
  
  function doors.Entrance:handlePlayerSpawn()
    self.playerAbove = false
    
    self.level.player = player.newPlayer(self.level,
                                         self.x + self.w/2 - 24 / 2, self.y - 24)
    self.level.player:disableMovement(self.openingAnimation.d)
    
    self.openingAnimation:setDirection('forward')
  end
  
  
  doors.Exit = {}; doors.Exit.__index = doors.Exit
  setmetatable(doors.Exit, doors.Door)
  function doors.newExit(level_, x, y)
    local self = doors.newDoor(level_, x, y); setmetatable(self, doors.Exit)
    
    self.autoOpenDistance = 96 -- manhattan distance
    
    return self
  end
  
  function doors.Exit:update()
    doors.Door.update(self)
    
    local p = self.level.player
    
    -- open if player is nearby
    local d = self.autoOpenDistance
    
    
    if self.playerAbove then
      -- open if player is nearby
      if rectangles.collide(self.x + self.w/2 - d, self.y+self.h/2 - d, d * 2, d * 2, p.x, p.y, p.w, p.h) then
        self.openingAnimation:setDirection('forward')
      else
        self.openingAnimation:setDirection('backward')
      end
      
      if rectangles.contains(self.x, self.y, self.w, self.h, p.x, p.y, p.w, p.h) then
        self.playerAbove = false
      end
    else
      if not self.level.completed then
        if not rectangles.collide(self.x, self.y, self.w, self.h, p.x, p.y, p.w, p.h) then
          -- level is completed
          
          p:disableMovement(math.huge)
          
          self.openingAnimation:setDirection('backward')
          
          self.level:complete()
        end
      end
    end
  end
    
return doors