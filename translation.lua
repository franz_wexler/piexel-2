local translation = {}

  local allTranslations = {
    en = {}, -- all strings passed to translation.get() are already in english
    eo = {
      ["Performance"] = "Rendimento",
      ["FPS"] = "FES", -- Framoj Ene Sekundo
      ["internal memory usage"] = "internmemoruzado",
      ["texture memory usage"] = "teksturmemoruzado",
      ["draw calls"] = "desegnado alvokoj",
      ["press Enter key"] = "premu Eniga klavo"
    },
    pl = {
      ["Performance"] = "Wydajność",
      ["FPS"] = "FPS", -- tego się nie tłumaczy
      ["texture memory usage"] = "zużyci pamięci tekstur",
      ["internal memory usage"] = "zużycie wewnętrznej pamięci",
      ["draw calls"] = "wywołania rysowania",
      ["press Enter key"] = "wciśnij klawisz Enter"
    }
  }
  
  local diacriticsRemoval = {
    ["ą"] = "a", ["Ą"] = "A",
    ["ć"] = "c", ["Ć"] = "C",
    ["ę"] = "e", ["Ę"] = "E",
    ["ł"] = "l", ["Ł"] = "L",
    ["ń"] = "n", ["Ń"] = "N",
    ["ó"] = "o", ["Ó"] = "O",
    ["ś"] = "s", ["Ś"] = "S",
    ["ź"] = "z", ["Ź"] = "Z",
    ["ż"] = "z", ["Ż"] = "Z",
    
    ["ĉ"] = "c", ["Ĉ"] = "C",
    ["ĝ"] = "g", ["Ĝ"] = "G",
    ["ĥ"] = "h", ["Ĥ"] = "H",
    ["ĵ"] = "j", ["Ĵ"] = "J",
    ["ŝ"] = "s", ["Ŝ"] = "S",
    ["ŭ"] = "u", ["Ŭ"] = "U"
  }
  
  local currentLanguage = 'eo'
  local removeDiacritics = true
  
  function translation.setLanguage(lang)
    currentLanguage = lang
  end
  function translation.getLanguage()
    return currentLanguage
  end
  function translation.switchLanguage()
    -- TERRIBLE function, but it is fun to switch languages
    local i = 1
    local j = nil
    for k, _ in pairs(allTranslations) do
      if k == currentLanguage then
        j = i + 1
      end
      i = i + 1
    end
    if j == i then j = 1 end
    i = 1
    for k, _ in pairs(allTranslations) do
      if i == j then
        currentLanguage = k
        return
      end
      i = i + 1
    end
  end

  function translation.get(text)
    local t = allTranslations[currentLanguage][text]
    if t == nil then
      t = text
    end
    
    if removeDiacritics then
      for from, to in pairs(diacriticsRemoval) do
        t = t:gsub(from, to)
      end
    end
    
    return t
  end

return translation