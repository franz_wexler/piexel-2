local transitionmanager = {}
  
  local lastState, currentState
  local transition = nil
  
  function transitionmanager.setState(newstate, transition_)
    if transition_ == nil then transition_ = transitionmanager.newFadeTransition() end
    
    lastState, currentState = currentState, newstate
    
    transition = transition_
  end
  
  function transitionmanager.update()
    if lastState ~= nil then
      if lastState ~= currentState then -- rarily they may be the same
        lastState:update()
      end
      
      local ended, callUpdate = transition:update()
      if callUpdate == nil then callUpdate = true end
      
      if ended then
        lastState = nil
        transition = nil
      end
      
      if callUpdate then
        currentState:update()
      end
    else
      currentState:update()
    end
  end
  
  function transitionmanager.draw()
    if lastState ~= nil then
      transition:draw()
    else
      currentState:draw()
    end
  end
  
  
  transitionmanager.SlideTransition = {}
  transitionmanager.SlideTransition.__index = transitionmanager.SlideTransition
  function transitionmanager.newSlideTransition(direction, time)
    if direction == nil then direction = 'w' end
    if time == nil then time = 45 end
    
    local self = {}; setmetatable(self, transitionmanager.SlideTransition)
    
    self.animX, self.animY = nil, nil
    if direction == 'n' then
      self.animY = animations.newAnimation(0, -576, time)
    elseif direction == 'e' then
      self.animX = animations.newAnimation(0, 1024, time)
    elseif direction == 's' then
      self.animY = animations.newAnimation(0, 576, time)
    elseif direction == 'w' then
      self.animX = animations.newAnimation(0, -1024, time)
    else
      error("Invalid direction")
    end
    
    return self
  end
  function transitionmanager.SlideTransition:update()
    if self.animX ~= nil then
      self.animX:update()
    end
    if self.animY ~= nil then
      self.animY:update()
    end
    return (self.animX == nil or self.animX:hasEnded()) and (self.animY == nil or self.animY:hasEnded())
  end
  function transitionmanager.SlideTransition:draw()
    local x, y = 0, 0
    if self.animX ~= nil then
      x = self.animX:getCurrentValue()
    end
    if self.animY ~= nil then
      y = self.animY:getCurrentValue()
    end
    local w, h = 1024, 576
    
    letterbox.setScissor(x, y, w, h)
    love.graphics.translate(x, y)
    lastState:draw()
        
    letterbox.setScissor(x + w, y, w, h)
    love.graphics.translate(w, 0)
    currentState:draw()
  end
  
  transitionmanager.FadeTransition = {}
  transitionmanager.FadeTransition.__index = transitionmanager.FadeTransition
  function transitionmanager.newFadeTransition(color, timeIn, timeOut)
    if color == nil then color = {0, 0, 0} end
    if timeIn == nil then timeIn = 45 end
    if timeOut == nil then timeOut = 45 end
    
    local self = {}; setmetatable(self, transitionmanager.FadeTransition)
    
    self.r, self.g, self.b = unpack(color)
    self.animIn = animations.newAnimation(0, 255, timeIn)
    self.animOut = animations.newAnimation(255, 0, timeOut)
    
    return self
  end
  function transitionmanager.FadeTransition:update()
    self.animIn:update()
    if self.animIn:hasEnded() then
      self.animOut:update()
    end
    return self.animOut:hasEnded(), self.animOut.t >= self.animOut.d / 4
  end
  function transitionmanager.FadeTransition:draw()
    local a
    if not self.animIn:hasEnded() then
      a = self.animIn:getCurrentValue()
      
      lastState:draw()
    else
      a = self.animOut:getCurrentValue()
      
      currentState:draw()
    end
    
    love.graphics.setColor(self.r, self.g, self.b, a)
    love.graphics.rectangle('fill', 0, 0, 1024, 576)
  end

return transitionmanager
