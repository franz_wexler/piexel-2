function love.conf(t)
  t.window.title = "Piexel 2: la Reveno de Rastrumero"
  
  t.window.width, t.window.height = 1024, 576
  t.window.resizable = true
  
  t.window.highdpi = true
  
  -- CHANGE IT IN RELEASE VERSIONS
  t.window.vsync = false
  
  t.modules.joystick = false
  t.modules.physics = false
  
end
