local player = {}

  player.Player = {}; player.Player.__index = player.Player
  
  function player.newPlayer(level_, x, y)
    if x == nil then x = 128 end
    if y == nil then y = 128 end
    
    local self = {}; setmetatable(self, player.Player)
    
    self.level = level_
    
    self.x, self.y, self.w, self.h = x, y, 24, 24
    self.velX, self.velY =  0, 0
    
    -- player has fancy splash animations, use these variables to get position and size of player for anything
    -- related with drawing
    self.drawX, self.drawY, self.drawW, self.drawH = self.x, self.y, self.w, self.h
    
    self.disabledMovementLeft = 0
    
    self.gravity = 0.3 -- px/frame^2
    self.jumpForce = 9 -- px/frame; player's velY is set to this when jumping
    self.movingSpeed = 5 -- px/frame; speed when walking
    self.maxVelX, self.maxVelY = 300, 300 -- maximal velocity, px/frame
    
    -- open to the only suggestion improvement I ever got for first part of Piexel, you can now hold SHIFT to sneak
    -- player's speed is multiplied by sneakingMultiplier when sneaking and on ground
    self.sneakingMultipler = 0.3
    
    -- true if player is touching something from one of these sides
    self.touchingUp, self.touchingLeft, self.touchingDown, self.touchingRight = false, false, false, false
    
    -- to make the game less frustrating, it is really forgetful
    -- if the player is touching something deadly, he doesn't die until deathTime has passed
    self.deathTime = 3
    self.deathTimeLeft = 0
    -- also, he can jump if time since he last time touched the ground is less than extraJumpTime
    self.extraJumpTime = 6
    self.extraJumpTimeLeft = 0
    
    self.jumpSplashAnimation = animations.newAnimation(2.5, 0, 28, animations.easeOut)
    self.jumpSplashAnimation.t = self.jumpSplashAnimation.d
    
    self.groundSplashAnimation = animations.newAnimation(0, -1.5, 6)
    self.groundSplashAnimation.paused = true
    
    self:update() -- should do more good than harm
    
    return self
  end
  
  function player.Player:update()
    local blocks = self.level.blocks
    local platforms = self.level.platforms
    local spikes = self.level.spikes
    local collide = rectangles.collide
    
    -- spikes collision
    local anyDeadlyCollision = false
    local deadlyCollisionReason = nil
    for i = 1, #spikes do
      local rx, ry, rw, rh = unpack(spikes[i])
      if collide(self.x, self.y, self.w, self.h, rx, ry, rw, rh) then
        anyDeadlyCollision = true
        deadlyCollisionReason = 'spike'
        break
      end
    end
    if anyDeadlyCollision then
      self.deathTimeLeft = self.deathTimeLeft - 1
      if self.deathTimeLeft <= 0 then
        self:kill(deadlyCollisionReason)
      end
    else
      self.deathTimeLeft = self.deathTime
    end
    
    
    -- keyboard input
    local goingLeft = love.keyboard.isScancodeDown('left') or love.keyboard.isScancodeDown('a')
    local goingRight = love.keyboard.isScancodeDown('right') or love.keyboard.isScancodeDown('d')
    local goingUp = love.keyboard.isScancodeDown('up') or love.keyboard.isScancodeDown('w') or
        love.keyboard.isScancodeDown('space')
    local goingDown = love.keyboard.isScancodeDown('down') or love.keyboard.isScancodeDown('s')
    local sneaking = love.keyboard.isScancodeDown('lshift') or love.keyboard.isScancodeDown('rshift')
    
    self.disabledMovementLeft = math.max(self.disabledMovementLeft - 1, 0)
    local canMove = (self.disabledMovementLeft == 0)
    
    -- because of way the physics are handled, x and y axis need to be handled separately, x first, y second
    
    -- x axis
    
    -- x velocity
    self.velX = 0
    if canMove then
      if goingLeft then
        self.velX = self.velX - self.movingSpeed
      end
      if goingRight then
        self.velX = self.velX + self.movingSpeed
      end
      
      -- sneaking
      if sneaking and self.touchingDown then
        self.velX = self.velX * self.sneakingMultipler
      end
    end
    
    -- move (x)
    self.x = self.x + self.velX
    
    -- solid blocks (x)
    if self.velX ~= 0 then
      for i = 1, #blocks do
        local rx, ry, rw, rh = unpack(blocks[i])
        if collide(self.x, self.y, self.w, self.h, rx, ry, rw, rh) then
          if self.velX > 0 then
            self.x = rx - self.w
          else
            self.x = rx + rw
          end
          self.velX = 0
          break
        end
      end
    end
    
    -- y axis
    
    local oldVelY = self.velY
    
    -- gravity acceleration
    self.velY = self.velY + self.gravity
    
    self.extraJumpTimeLeft = math.max(0, self.extraJumpTimeLeft - 1)
    
    -- jump
    if canMove then
      if goingUp and self.extraJumpTimeLeft > 0 then
        self.velY = self.velY - self.jumpForce
        self.extraJumpTimeLeft = 0
        
        self.jumpSplashAnimation.t = 0
        self.jumpSplashAnimation.paused = false
      end
    end
    
    -- move (y)
    self.y = self.y + self.velY
    
    -- solid blocks (y)
    self.touchingDown = false
    for i = 1, #blocks do
      local rx, ry, rw, rh = unpack(blocks[i])
      if collide(self.x, self.y, self.w, self.h, rx, ry - 1, rw, rh + 1) then
        if self.velY > 0 then
          self.y = ry - self.h
          self.touchingDown = true
          
          self.extraJumpTimeLeft = self.extraJumpTime
        else -- self.velY <= 0
          self.y = ry + rh
          self.touchingUp = true
          
          self.jumpSplashAnimation.t = self.jumpSplashAnimation.d
        end
        self.velY = 0
        break
      end
    end
    
    -- platforms
    if self.velY > 0 then
      for i = 1, #platforms do
        local rx, ry, rw, rh = unpack(platforms[i])
        if collide(self.x, self.y, self.w, self.h, rx, ry - 1, rw, rh + 1) then
          if self.y + self.h - self.velY * 2 <= ry then
            if not goingDown then
              self.y = ry - self.h
            end
            self.touchingDown = true
            self.extraJumpTimeLeft = self.extraJumpTime
            self.velY = 0
          end
        end
      end
    end
    
    if oldVelY > 0 and self.velY == 0 and not goingUp then
      self.groundSplashAnimation:setDirection('forward')
      self.groundSplashAnimation.paused = false
    end
    
    
    local jsa = self.jumpSplashAnimation
    local gsa = self.groundSplashAnimation
    
    -- update splash animations
    jsa:update()
    gsa:update()
    if gsa:hasEnded() then
      gsa:setDirection('backward')
    end
    
    -- update fancy position and size
    local v = jsa:getCurrentValue()
    local px, py, pw, ph = self.x, self.y, self.w, self.h
    if v ~= 0 then
      px, py, pw, ph = px + v, py - v, pw - 2 * v, ph + 2 * v
    else
      v = gsa:getCurrentValue()
      if v ~= 0 then
        px, py, pw, ph = px + v, py - 2 * v, pw - 2 * v, ph + 2 * v
      end
    end
    
    -- clamp player to be inside doors
    for _, d in ipairs({self.level.entrance, self.level.exit}) do
      if not d.playerAbove then
        local dv = d.openingAnimation:getCurrentValue()
        local rx, ry, rw, rh = d.x + d.w / 2 - dv, d.y, dv * 2, d.h
        px, py, pw, ph = rectangles.clamp(px, py, pw, ph, rx, ry, rw, rh)
      end
    end
      
    
    self.drawX, self.drawY, self.drawW, self.drawH = px, py, pw, ph
  end
  
  function player.Player:kill(reason)
    if reason == nil then reason = '<unknown>' end
    
    self.level.playerDeathParticles:addPlayerDeathParticles(self)
    
    self.level.entrance:handlePlayerSpawn()
  end
  
  function player.Player:draw()
    
    local px, py, pw, ph = self.drawX, self.drawY, self.drawW, self.drawH 
    
    love.graphics.setColor(72, 92, 245)
    love.graphics.rectangle('fill', px, py, pw, ph)
    love.graphics.setColor(self.level.theme.playerColor)
    love.graphics.rectangle('fill', px + pw * 0.1, py + ph * 0.1, pw * 0.8, ph * 0.8)
  end
  
  function player.Player:disableMovement(time)
    self.disabledMovementLeft = math.max(self.disabledMovementLeft, time)
  end
  
  function player.Player:getSegments()
    return shadows.getRectangleSegments(self.drawX, self.drawY, self.drawW, self.drawH)
  end
  
return player
