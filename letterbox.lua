local letterbox = {}
  
  local sceneW, sceneH
  local drawScale, viewX, viewY, viewW, viewH
  local canvas

  function letterbox.getSceneSize()
    return sceneW, sceneH
  end
  function letterbox.setSceneSize(w, h) -- should be called in love.load()
    sceneW, sceneH = w, h
    letterbox.resize()
  end

  function letterbox.resize(w, h) -- should be called in love.resize()
    if w == nil then w = love.graphics.getWidth() end
    if h == nil then h = love.graphics.getHeight() end
    
    drawScale = math.min(w / sceneW, h / sceneH)
    
    viewW, viewH = sceneW * drawScale, sceneH * drawScale
    
    canvas = love.graphics.newCanvas(viewW, viewH)
    
    viewX, viewY = (w - viewW) / 2, (h - viewH) / 2
  end

  function letterbox.beginDrawing() -- should be called at beginning of love.draw()
    love.graphics.push()
    love.graphics.scale(drawScale, drawScale)
    love.graphics.setCanvas(canvas)
  end

  function letterbox.endDrawing() -- should be called at end of love.draw()
    love.graphics.pop()
    love.graphics.setCanvas(nil)
    love.graphics.setColor(255, 255, 255, 255)
    
    love.graphics.draw(canvas, viewX, viewY)
  end
  
  function letterbox.getViewSize()
    return viewW, viewH
  end


  function letterbox.translatePos(x, y)
    x, y = x - viewX, y - viewY
    x, y = x / drawScale, y / drawScale
    return math.floor(x), math.floor(y)
  end
  function letterbox.getMousePosition()
    return letterbox.translatePos(love.mouse.getPosition())
  end
  
  function letterbox.setScissor(x, y, w, h)
    x, y = viewX + x * drawScale, viewY + y * drawScale
    w, h = w * drawScale, h * drawScale
    love.graphics.setScissor(x, y, w, h)
  end
  
  
  local loadedFonts = {}
  function letterbox.print(size, text, x, y, limitx, limity, alignx, aligny, r)
    if x == nil then x = 0 end
    if y == nil then y = 0 end
    if limitx == nil then limitx = 1024 end
    if limity == nil then limity = 576 end
    if alignx == nil then alignx = 'center' end
    if aligny == nil then aligny = 'center' end
    if r == nil then r = 0 end
    
    size = math.floor(size * drawScale) -- love2d rounds it down anyway
    
    local font = loadedFonts[size]
    if font == nil then
      loadedFonts[size] = love.graphics.newFont("rise.ttf", size)
      font = loadedFonts[size]
    end
    
    if aligny ~= 'top' then
      local _, wrapped = font:getWrap(text, limitx)
      local numlines = #wrapped
      local height = numlines * font:getHeight() * (1 / drawScale)
      if aligny == 'center' then
        y = y + limity / 2 - height / 2
      else -- aligny == 'bottom'
        y = y + limity - height
      end
    end
    
    love.graphics.setFont(font)
    love.graphics.printf(text, x, y, limitx * drawScale, alignx,
      r, 1 / drawScale, 1 / drawScale)
  end

return letterbox
