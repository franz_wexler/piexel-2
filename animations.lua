local animations = {}

  -- easing functions
  
  function animations.linear(t, b, c, d)
    return c * t / d + b
  end
  
  function animations.easeOut(t, b, c, d)
    return c * math.pow(t / d, 2) + b
  end
  
  function animations.easeInOut(t, b, c, d)
    t = t / d * 2
    if t < 1 then
      return c / 2 * t * t + b
    end
    return -c / 2 * ((t - 1) * (t - 3) - 1) + b
  end
  
  -- Animation class for high-level managing of animations

  -- newAnimation(beginValue: number, endValue: number, time: number = 60,
  --              easingFunction: function(t, b, c, d) = easeInOut)
  animations.Animation = {}; animations.Animation.__index = animations.Animation
  function animations.newAnimation(beginValue, endValue, time, easingFunction)
    if time == nil then time = 120 end
    if easingFunction == nil then easingFunction = animations.easeInOut end
    
    local self = {}; setmetatable(self, animations.Animation)
    
    self.t = 0 -- current time of animation
    self.b = beginValue -- begin value
    self.c = endValue - beginValue -- difference between begin and end
    self.d = time -- total time of animation
    
    self.func = easingFunction -- easing function
    
    self.direction = 'forward' -- should be 'forward' or 'backward'
    self.paused = false -- if true, update() won't do anything
    
    return self
  end
  
  function animations.Animation:update(step)
    if step == nil then step = 1 end
    
    if not self.paused then
      self.t = self.t + step
      if self.t >= self.d then
        self.t = self.d
      elseif self.t < 0 then -- may happen if step is negative
        self.t = 0
      end
    end
  end
  
  function animations.Animation:getDirection()
    return self.direction
  end
  function animations.Animation:setDirection(direction)
    if direction ~= self.direction then
      self.t = self.d - self.t
    end
    
    self.direction = direction
  end
  function animations.Animation:reverseDirection()
    if self.direction == 'forward' then self:setDirection('backward')
    else self:setDirection('forward') end
  end
  
  function animations.Animation:hasEnded()
    return self.t == self.d
  end
  
  function animations.Animation:getCurrentValue()
    local v = self.func(self.t, self.b, self.c, self.d)
    if self.direction == 'backward' then
      v = self.b * 2 + self.c - v
    end
    return v
  end

return animations