local rectangles = {}

  function rectangles.collidepoint(x, y, w, h, px, py)
    return x < px and px < x + w and y < py and py < y + h
  end

  function rectangles.collide(x1, y1, w1, h1, x2, y2, w2, h2)
    return x1 < x2 + w2 and x1 + w1 > x2 and y1 < y2 + h2 and y1 + h1 > y2
  end
  
  function rectangles.contains(x1, y1, w1, h1, x2, y2, w2, h2)
    return x1 < x2 and y1 < y2 and x1 + w1 >= x2 + w2 and y1 + h1 >= y2 + h2
  end
  
  function rectangles.inflate(x1, y1, w1, h1, x2, y2, w2, h2)
    local x, y = math.min(x1, x2), math.min(y1, y2)
    return x, y, math.max(x1 + w1, x2 + w2) - x, math.max(y1 + h1, y2 + h2) - y
  end
  
  function rectangles.clamp(x1, y1, w1, h1, x2, y2, w2, h2)
    if x1 < x2 then
      w1 = math.max(w1 - (x2 - x1), 0)
      x1 = x2
    end
    if y1 < y2 then
      h1 = math.max(h1 - (y2 - y1), 0)
      y1 = y2
    end
    w1 = math.min(w1, math.max(x2 + w2 - x1, 0))
    h1 = math.min(h1, math.max(y2 + h2 - y1, 0))
    return x1, y1, w1, h1
  end
  
  function rectangles.optimize(rectlist)
    -- This function magically works. Do not try to optimize it, it only gets worse
    -- Uses some hacks and I have no idea how it works on the same table without errors
    local changed = true
    while changed do
      changed = false
      for _, r1 in ipairs(rectlist) do
        for _, r2 in ipairs(rectlist) do
          local x1, y1, w1, h1 = unpack(r1)
          local x2, y2, w2, h2 = unpack(r2)
          if x1 == x2 and x1 + w1 == x2 + w2 and (y1 == y2 + h2 or y2 == y1 + h1) then
            changed = true
            r1[1], r1[2], r1[3], r1[4] = rectangles.inflate(x1, y1, w1, h1, x2, y2, w2, h2)
            table.removeitem(rectlist, r2)
          end
        end
      end
    end
  end
  
  

return rectangles
