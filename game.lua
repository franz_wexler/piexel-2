local game = {}

  game.ticks = 0
  game.defaultFont = nil
  
  function game.load()
    levelmanager.loadFolder('levels/')
    shadermanager.loadShaders()
    
    letterbox.setSceneSize(64 * 16, 36 * 16)
    
    game.defaultFont = love.graphics.getFont()
    
    UPDATETIME = 1 / 60
    local dti = 0
    function love.update(dt)
      -- Update interval can't be too low or too high
      dti = dti + dt
      
      while dti > UPDATETIME do
        if not love.keyboard.isDown('q') then
          game.update()
        end
        dti = dti - UPDATETIME
      end
      
      if love.keyboard.isDown('u') then
        love.graphics.setWireframe(true)
      end
    end
    
    function love.keyreleased(key)
      if key == 'e' then
        game.update(0.015)
      elseif key == 'h' then
        levelmanager.nextLevel()
      elseif key == 'i' then
        translation.switchLanguage()
      end
    end
    
    function love.draw()
      letterbox.beginDrawing()
      game.draw()
      shadermanager.setShader('distort')
      letterbox.endDrawing()
      shadermanager.setShader(nil)
      
      local trns = translation.get
    
      local stats = love.graphics.getStats()
      love.graphics.setColor(255, 255, 255)
      love.graphics.setFont(game.defaultFont)
      love.graphics.print("["..trns("Performance").."]"..
        "  "..trns("FPS").."="..love.timer.getFPS()..
        "  "..trns("internal memory usage").."="..math.floor(collectgarbage('count')).."kB"..
        "  "..trns("texture memory usage").."="..math.floor(stats.texturememory/1000).."kB"..
        "  "..trns("draw calls").."="..stats.drawcalls)
    end
    function love.resize(w, h)
      letterbox.resize(w, h)
    end
  end
  
  function game.update()
    game.ticks = game.ticks + 1
    shadermanager.update()
    transitionmanager.update()
  end
  
  function game.draw()
    transitionmanager.draw()
  end
  
return game
